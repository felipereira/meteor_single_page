Pedidos = new Mongo.Collection("pedidos");
Produtos = new Mongo.Collection("produdos");

Meteor.methods({
  finalizar: function (id) {
    Pedidos.update(id, {
      $set: { ativo: false }
    });
  }
});

if (Meteor.isServer) {

  Meteor.publish("pedidos", function () {
    var lastHour = new Date();
    lastHour.setHours(lastHour.getHours() - 1);

    return Pedidos.find({
      "ativo": true,
      "createdAt": { $gt: lastHour }
    });
  });

  Picker.route('/novo-pedido', function (params, req, res, next) {

    res.setHeader('Content-Type', 'text/plain; charset=UTF-8');
    res.setHeader('Access-Control-Allow-Origin', '*');

    if (!params.query.mesa && !params.query.info) {
      res.statusCode = 400;
      res.end('Faltou definir a Mesa');
    } else if (!params.query.pedido) {
      res.statusCode = 400;
      res.end('Faltou incluir os itens do pedido');
    } else {

      var pedido = Pedidos.insert({
        mesa: params.query.mesa,
        info: params.query.info,
        itens: params.query.pedido,
        observacao: params.query.observacao,
        ativo: true,
        userAgent: req.headers['user-agent'],
        createdAt: new Date()
      });

      res.statusCode = 200;
      res.end('Pedido cadastrado!');

    }

  });

  Picker.route('/novo-produto', function (params, req, res, next) {

    res.setHeader('Content-Type', 'text/plain; charset=UTF-8');
    res.setHeader('Access-Control-Allow-Origin', '*');

    var produdos = Produtos.insert({
      nome: params.query.nome,
      descricao: params.query.descricao,
      preco: params.query.preco,
      codigo: params.query.codigo
    });

    res.statusCode = 200;
    res.end('Produto cadastrado');

  })

  Picker.route('/produtos-cadastrados', function (params, req, res, next) {

    res.setHeader('Content-Type', 'text/plain; charset=UTF-8');
    res.setHeader('Access-Control-Allow-Origin', '*');

    var produdosCadastrados = Produtos.find().fetch();

    if (!!params.query.random) {
      produdosCadastrados = produdosCadastrados.concat(produdosCadastrados);
      shuffle(produdosCadastrados);
    }

    res.end(JSON.stringify(produdosCadastrados));

  })

  Picker.route('/historico-pedidos', function (params, req, res, next) {

    res.setHeader('Content-Type', 'application/json; charset=UTF-8');
    res.setHeader('Access-Control-Allow-Origin', '*');

    var resposta = Pedidos.find().fetch();

    if (!!params.query.random) {
      resposta = resposta.concat(resposta);
      shuffle(resposta);
    }

    res.end(JSON.stringify(resposta));
  });

}


if (Meteor.isClient) {
  Meteor.subscribe("pedidos");

  Template.body.helpers({
    pedidos: function () {
      return Pedidos.find({}, {
        sort: { createdAt: -1 }
      });
    }
  });

  Template.produdo.player = function(){
    return ''
  }

  Template.pedido.events({
    "click .delete": function () {
      Meteor.call("finalizar", this._id);
    }
  });

  Template.registerHelper('horario', function (date) {
    return (date.getHours() + 100 + '').substr(1) + ':' + (date.getMinutes() + 100 + '').substr(1);
  });

}

function shuffle(array) {
  var counter = array.length, temp, index;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}
